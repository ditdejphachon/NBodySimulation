
#include "CTNBodyManager.h"
#include "Components/InstancedStaticMeshComponent.h"
#include <Components/BoxComponent.h>
// Sets default values
ACTNBodyManager::ACTNBodyManager()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.TickGroup = TG_DuringPhysics;

	Boundary = CreateDefaultSubobject<UBoxComponent>(TEXT("Boundary"));
	RootComponent = Boundary;

	InstancedMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("InstancedMesh"));
	InstancedMesh->SetupAttachment(RootComponent);
}

void ACTNBodyManager::OnConstruction(const FTransform& Transform)
{
	//InitSim();
}

void ACTNBodyManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateGravityStep(DeltaTime);
	UpdatePosition(DeltaTime);

	if (m_CanShrink)
	{
		FVector BoxExtent = Boundary->GetUnscaledBoxExtent();
		BoxExtent.X -= ShrinkAmount;
		BoxExtent.Y -= ShrinkAmount * 1.78f;
		if (BoxExtent.X < MinShrinkAmount || BoxExtent.Y < MinShrinkAmount)
		{
			BoxExtent.X = m_InitialBoundaryWidth ;
			BoxExtent.Y = m_InitialBoundaryHeight;

			m_CanShrink = false;
			FTimerHandle TimerHandle;
			FTimerDelegate TimerDelegate;
			TimerDelegate.BindLambda([&]() {
				m_CanShrink = true;
				});
			GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, CanShrinkDelay, false);
		}
		m_BoundaryWidth = BoxExtent.X;
		m_BoundaryHeight = BoxExtent.Y;
		Boundary->SetBoxExtent(BoxExtent);
	}
	
}

void ACTNBodyManager::BeginPlay()
{
	Super::BeginPlay();
	InitSim();
}

void ACTNBodyManager::InitSim()
{
	InstancedMesh->ClearInstances();
	NBodyInfos.Empty();

	if (Boundary)
	{
		FVector BoxExtent = Boundary->GetUnscaledBoxExtent();
		m_BoundaryWidth = m_InitialBoundaryWidth =  BoxExtent.X;
		m_BoundaryHeight = m_InitialBoundaryHeight = BoxExtent.Y;
	}

	// init body info
	for (int i = 0; i < BodyCount; i++)
	{
		FVector2D RandomSpawnPosition;
		RandomSpawnPosition.X = FMath::FRandRange(-m_BoundaryWidth  /2, m_BoundaryWidth  /2);
		RandomSpawnPosition.Y = FMath::FRandRange(-m_BoundaryHeight /2, m_BoundaryHeight /2);
		FTransform transform( FRotator(), ToVec3(RandomSpawnPosition), FVector(1.0f, 1.0f, 1.0f));

		FVector2D RandomVelocity(FMath::RandPointInCircle(Speed));
		float Mass = FMath::FRandRange(MinMass, MaxMass);

		NBodyInfos.Add(FCTNBodyInfo{ RandomSpawnPosition , transform, RandomVelocity, Mass });
	}

	// add instances
	for (const auto& info : NBodyInfos)
	{
		InstancedMesh->AddInstance(info.Transform, false);
	}
}

void ACTNBodyManager::UpdateGravityStep(float DeltaTime)
{
	float MaxGravityDistance = m_BoundaryWidth;

	for (int i = 0; i < NBodyInfos.Num(); i++)
	{
		FVector2D Acceleration(0.0f, 0.0f);
		for (int j = 0; j < NBodyInfos.Num(); j++)
		{
			if (i == j) continue;
			float distance = FVector2D::Distance(NBodyInfos[i].Position, NBodyInfos[j].Position);
			distance = FMath::Max(distance, MinimumGravityDistance);
			Acceleration += NBodyInfos[j].Mass / distance * G / distance * (NBodyInfos[j].Position - NBodyInfos[i].Position) / distance;
		}
		NBodyInfos[i].Velocity += Acceleration * DeltaTime;
	}
}

void ACTNBodyManager::UpdatePosition(float DeltaTime)
{
	int32 i = 0;
	for (auto& info : NBodyInfos)
	{
		info.Position += info.Velocity * DeltaTime;

		if (info.Position.X < -(m_BoundaryWidth))
		{
			info.Position.X += m_BoundaryWidth * 2;
		}
		else if (info.Position.X > (m_BoundaryWidth))
		{
			info.Position.X -= m_BoundaryWidth * 2;
		}

		if (info.Position.Y < -(m_BoundaryHeight))
		{
			info.Position.Y += m_BoundaryHeight * 2;
		}
		else if (info.Position.Y > (m_BoundaryHeight))
		{
			info.Position.Y -= m_BoundaryHeight * 2;
		}

		info.Transform.SetTranslation(ToVec3(info.Position));

		InstancedMesh->BatchUpdateInstancesTransform(i, 1, info.Transform, false, true);
		i++;
	}
}

FVector ACTNBodyManager::ToVec3(const FVector2D& inVec2)
{
	return FVector(inVec2.X, inVec2.Y, 0.0f);
}
