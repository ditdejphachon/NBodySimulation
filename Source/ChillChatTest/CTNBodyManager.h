// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CTNBodyManager.generated.h"

class ATriggerVolume;
class UBoxComponent;
class UInstancedStaticMeshComponent;

USTRUCT()
struct FCTNBodyInfo
{
	GENERATED_BODY()
		
	FCTNBodyInfo() {};
	FCTNBodyInfo(FVector2D inPosition, FTransform inTransform, FVector2D inVelocity, float inMass)
		: Position(inPosition)
		, Transform(inTransform)
		, Velocity(inVelocity)
		, Mass(inMass)
	{}

	FVector2D Position;
	FTransform Transform;
	FVector2D Velocity;
	float Mass;
};

UCLASS()
class CHILLCHATTEST_API ACTNBodyManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACTNBodyManager();
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere) TObjectPtr<UBoxComponent> Boundary;
	UPROPERTY(EditAnywhere, Instanced, NoClear) UInstancedStaticMeshComponent* InstancedMesh;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions", meta = (ClampMin = "1", ClampMax = "10000")) int BodyCount = 100;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float Speed = 100.0f;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float MinimumGravityDistance = 100.0f;  // prevents division by zero and forces too high	
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float G = 1000.0f;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float MinMass = 20.0f;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float MaxMass = 100.0f;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float ShrinkAmount = 10.0f;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float MinShrinkAmount = 10.0f;
	UPROPERTY(EditAnywhere, Category = "CT|Conditions") float CanShrinkDelay = 2.0f;

protected:
	virtual void BeginPlay() override;
	void InitSim();
	void UpdateGravityStep(float DeltaTime);
	void UpdatePosition(float DeltaTime);

protected:
	TArray<FCTNBodyInfo> NBodyInfos;

private:
	FVector ToVec3(const FVector2D& inVec2);
private:
	float m_BoundaryWidth;
	float m_BoundaryHeight;
	float m_InitialBoundaryWidth;
	float m_InitialBoundaryHeight;
	bool m_CanShrink = true;
};
