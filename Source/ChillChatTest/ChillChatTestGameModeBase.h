// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ChillChatTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHILLCHATTEST_API AChillChatTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
